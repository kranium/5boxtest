import Shellish
import Text.Printf

linemax = 100
linerange = [1..linemax]
modpairs = map (\n -> (n,linemax-n)) [1..linemax-1]
addtext = "add more text"
addfilename = printf "test/add_%03d"
delfilename = printf "test/del_%03d"
modfilename = printf "test/mod_%03d-%03d"

gitCommit msg = shellish $ do
  setenv "GIT_AUTHOR_NAME" "Kranium Gikos Mendoza"
  setenv "GIT_AUTHOR_EMAIL" "womfoo@gmail.com"
  run "git" ["add", "test/*"]
  run "git" ["commit", "-m " ++ msg]

main = do 
  shellish $ run "mkdir" ["test"]
  mapM_ (\a -> writeFile (addfilename a) "") linerange
  mapM_ (\d -> writeFile (delfilename d) (unlines (replicate d (delfilename d)))) linerange
  mapM_ (\(a,d) -> writeFile (modfilename a d) (unlines (replicate d (modfilename a d)))) modpairs
  shellish $ run "git" ["init"]
  gitCommit "starting point for files"

  mapM_ (\a -> writeFile (addfilename a) (unlines (replicate a addtext))) linerange
  gitCommit "add n"

  mapM_ (\d -> writeFile (delfilename d) "") linerange
  gitCommit "del n"

  mapM_ (\(a,d) -> writeFile (modfilename a d) (unlines (replicate a addtext))) modpairs
  gitCommit "mod adds dels"
